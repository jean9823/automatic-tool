#!/bin/bash

set -ex

IMG="openEuler-D1-xfce-docker.img.bz2"
TF="/dev/sdb"

bzcat ${IMG} '|'| sudo dd of=${TF} bs=1M iflag=fullblock oflag=direct conv=fsync status=progress