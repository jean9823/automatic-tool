#!/bin/bash

set -ex

echo $#   #返回传递到脚本的参数个数
echo $1   #shell脚本传的第一个参数



die () { echo "$*" ; exit 3 ;}

[ $# -eq 1 ] || die "$0 disk ID"
DISK_N="$1"

#IMG=Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide-latest-sda.raw 
#
#[ -f "$IMG" ] || \
#unzstd  "${IMG}.zst" || \
#die "unzstd  Fedora-riscv64-d1-developer-xfce-with-esp-Rawhide-latest-sda.raw.zst failed"

#IMG=RVBoards_D1_Debian_lxde_img_linux_v0.5.img
IMG=openEuler-D1-xfce-docker.img


#while true; do
#	read -p "which disk? /dev/sd? >" DISK_N
	DISK="/dev/sd${DISK_N}"
	sudo wipefs -a "$DISK" || true
	sudo dd if="$IMG" of="$DISK" status=progress bs=4M
	#sudo sync
	echo "Done +1"

#done

# seq 9

# seq 0 9

# seq 0 9 | tr '0-9' 'c-z' 

# seq 0 9 | tr '0-9' 'c-z' | xargs -I{} echo umount /dev/sd{}*

# seq 0 9 | tr '0-9' 'c-z' | xargs -I{} echo umount /dev/sd{}* \; \\ ; echo echo GO-GO-GO