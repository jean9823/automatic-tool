import requests
from requests.auth import HTTPBasicAuth
import constant

def get_pkgs():
    with open('delete_pkg.txt') as f:
        pkglist = [line.replace('\n', '') for line in f.readlines()]
        # print ('pkglist:', pkglist) 
    return pkglist

def delete_pkg(pkglist):
    for pkg in pkglist:
        delete_pkg_url = '{}/source/{}/{}'.format(url, project, pkg)
        resp = requests.delete(delete_pkg_url, auth=HTTPBasicAuth(account['user'],account['password']))
        # print ('resp.status_code', resp.status_code)
        if resp.status_code == 200:
            print ('{} delete ok'.format(pkg))
        else:
            print ('{} delete error'.format(pkg))


if __name__=="__main__":
    pkglist = get_pkgs()
    url = constant.obs_url
    project = constant.obs_project
    account = constant.obs_account
    delete_pkg(pkglist)