#### 1.脚本功能：

  该脚本实现的功能是批量删除obs指定项目中的指定包

#### 2.使用说明：

1)安装前先执行pip install -r requirements.txt安装执行该脚本所需的python第三方库
2)编辑delete_pkg.txt文件，将需要删除的包名写在里面，一个包名写一行
3)编辑contant.py文件，其中：
    obs_account: obs的账号
    obs_url: obs url，此处末尾不要加'/'
    obs_project: obs中的项目名称
4)执行命令python run.py实现批量删除obs指定项目中的指定包
