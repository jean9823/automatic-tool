import sys
sys.path.append('/home/ubuntu/.local/lib/python3.9/site-packages')
from get_deviceinfo import GetDeviceInfo
from operate_influxdb import OperateInfluxDB
from data_model import DataBaseInfo
import schedule


def job():
    getDeviceInfo = GetDeviceInfo()
    deviceinfo = getDeviceInfo.get_deviceinfo()
    # print (deviceinfo.hostname, deviceinfo.ip_addr, deviceinfo.cpu_utilization, deviceinfo.mem_total, deviceinfo.mem_free, deviceinfo.mem_percent, deviceinfo.mem_used, deviceinfo.timestamp)
    databaseinfo = DataBaseInfo()
    # print ('11111', 'databaseinfo.measurement', databaseinfo.url, databaseinfo.org, databaseinfo.bucket, databaseinfo.token)
    operateDB = OperateInfluxDB(databaseinfo)
    # print ('22222', 'databaseinfo.measurement', databaseinfo.url, databaseinfo.org, databaseinfo.bucket, databaseinfo.token)
    operateDB.write_db(deviceinfo)
    print ('111111')
    # operateDB.read_db()


# schedule.every(10).seconds.do(job)
schedule.every(10).minutes.do(job)
# schedule.every(2).minutes.do(job)

if __name__ == '__main__':
  while True:
    schedule.run_pending()



# getDeviceInfo = GetDeviceInfo()
# deviceinfo = getDeviceInfo.get_deviceinfo()
# print (deviceinfo.measurement, deviceinfo.hostname, deviceinfo.ip_addr, deviceinfo.cpu_utilization, deviceinfo.mem_total, deviceinfo.mem_free, deviceinfo.mem_percent, deviceinfo.mem_used, deviceinfo.timestamp)
# databaseinfo = DataBaseInfo()
# print (databaseinfo.url, databaseinfo.org, databaseinfo.bucket, databaseinfo.token)
# operateDB = OperateInfluxDB(databaseinfo)
# # print ('22222', databaseinfo.url, databaseinfo.org, databaseinfo.bucket, databaseinfo.token)
# operateDB.write_db(deviceinfo)
