import csv
import os
import pandas as pd

header = ['id', 'name', 'age']

data = [
    [1, 'aa', 13],
    [2, 'bb', 15],
    [3, 'cc', 16],
]

a = [1,2,3]
b = [4,5,6]
dataframe = pd.DataFrame({'a_name':a,'b_name':b})

filepath = os.path.join(os.getcwd(), 'test.csv')

# if os.path.exists(filepath):
#         os.remove(filepath)

# with open(filepath,"w") as csvfile: 
#     writer = csv.writer(csvfile)
#     writer.writerow(header)
#     writer.writerows(data)

dataframe.to_csv(filepath,index=False)