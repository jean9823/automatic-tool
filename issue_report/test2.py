import requests
import json
import csv
import datetime
import time


issue_url = 'https://api.github.com/repos/plctlab/openEuler-riscv/issues'
username = 'jiewu-plct'
i = 1
issue_list = []
while True:
    headers = {'Accept': 'application/vnd.github.v3+json'}
    params = {'page': i, 'direction': 'asc'}
    response = requests.get(issue_url, headers=headers, params=params, auth=('jiewu-plct', 'jean801106'))
    issues = json.loads(response.text)
    print ('issues length', len(issues))
    if len(issues) > 0:
        issue_list = issue_list + issues
        i = i+1 
    else:
        break
print (len(issue_list))


# print (issue_list[0]['created_at'])
# utc = '2021-08-29T03:03:12Z'
def get_localtime(utc_time):
    utc_time = datetime.datetime.strptime(utc_time, '%Y-%m-%dT%H:%M:%SZ')
    now_stamp = time.time()
    local_st = datetime.datetime.fromtimestamp(now_stamp)
    utc_st = datetime.datetime.utcfromtimestamp(now_stamp)
    offset = local_st - utc_st
    local_time = utc_time + offset
    local_time = local_time.strftime('%Y-%m-%d %H:%M:%S')
    print (local_time, type(local_time))
    return local_time

# get_localtime(utc)

def get_comments(id):
    username = 'jiewu-plct'
    comment_url ='https://api.github.com/repos/plctlab/openEuler-riscv/issues/{}/comments'.format(id)
    comment_list = []
    i = 1
    while True:
        headers = {'Accept': 'application/vnd.github.v3+json'}
        params = {'page': i}
        response = requests.get(comment_url, headers=headers, params=params, auth=('jiewu-plct', 'jean801106'))
        comments = json.loads(response.text)
        print ('comments length', len(comments))
        if len(comments) > 0:
           comment_list = comment_list + comments
           i = i+1 
        else:
           break
    print (len(comment_list))
    return comment_list

# comments = get_comments(25)
# # print(comments)
# last_time = get_localtime(comments[-1]['created_at'])
# last_member = comments[-1]['user']['login']
# print (last_time, last_member)

issue_table = []
for issue in issue_list:
    issue_id = issue['number']
    title = issue['title']
    labels_list = [label['name'] for label in issue['labels']]
    labels = ','.join(labels_list)
    issue_link = issue['url']
    created_time = get_localtime(issue['created_at'])
    assignees_list = [member['login'] for member in issue['assignees']]
    assignees = ','.join(assignees_list)
    if issue['comments'] > 0:
        comments_list = get_comments(issue['number'])
        lastcomment_time = get_localtime(comments_list[-1]['created_at'])
        lastcomment_member = comments_list[-1]['user']['login']
    else:
        lastcomment_time = ''
        lastcomment_member = ''
    issue_data = [issue_id, title, labels, issue_link, created_time, assignees, lastcomment_time, lastcomment_member]
    issue_table.append(issue_data)

print (len(issue_table))
print (issue_table[0])
table_header = ['ID', 'Title', 'Issue Link', 'Created Time', 'Assignees', 'Time For Last Comment', 'Member For Last Comment']