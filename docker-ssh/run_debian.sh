#!/bin/bash

set -ex

sshport='3000'
# docker build -t debian_ssh_riscv:v1.1 -f Dockerfile_Debian .
docker run -dit -p ${sshport}:22 -v /root/docker_debian:/home/share/ --restart=always --name debian_ssh kr1510/debian_ssh_riscv:v1.1