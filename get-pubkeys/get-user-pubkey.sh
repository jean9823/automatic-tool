#!/bin/bash

set -ex


# personal_token="<your personal token>"
# personal_token="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"
githubID="jiewu-plct"
containername="ubuntu_${githubID}"


if [ ! -d "${githubID}_pubkey" ]; then
  mkdir ${githubID}_pubkey
fi

# curl -H "Authorization: token ${personal_token}" https://github.com/${githubID}.keys > ./${githubID}_pubkey/id_rsa.pub

curl https://github.com/${githubID}.keys > ./${githubID}_pubkey/id_rsa.pub

docker cp ./${githubID}_pubkey/id_rsa.pub ${containername}:/home/tester/.ssh/id_rsa.pub