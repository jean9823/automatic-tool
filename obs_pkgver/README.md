#### 脚本功能

获取obs指定工程中指定包的版本信息

#### 使用方法

1. 执行 pip install -r requirements.txt 安装执行该脚本所需的python第三方库

2. 在 parameters.py 文件中设置以下参数

   ````
   obs_account = {'username':'your username of obs account', 'password':'your password of obs account'}
   obs_url = 'https://build.tarsier-infra.com'
   obs_project = 'openEuler:23.03'
   obs_repo = '23.02'
   obs_packagelist = ['adcli', 'python-docker']
   ````

   obs_account: obs账号的用户名和密码

   obs_url: obs url

   obs_project: 要获取版本信息的包所在的obs工程

   obs_repo: 要获取版本信息的包所在的obs仓库

   obs_packagelist: 要获取版本信息的包列表

3. 执行 python get_obs_pkgver.py 运行脚本

   说明：get_obs_pkgver.py中的2个函数：

   get_obs_rpmver(): 根据构建成功的rpm包名获取版本信息

   get_obs_specver(): 根据spec文件里的内容获取版本信息

   可以根据自己的需要选择使用那种方式获取
