import requests
from requests.auth import HTTPBasicAuth
import parameters
import re


def get_obs_rpmver(url, account, project, repo, packages):
    for pkg in packages:
        rpm_url = '{}/build/{}/{}/riscv64/{}'.format(url, project, repo, pkg)
        rpm_resp = requests.get(rpm_url,auth=HTTPBasicAuth(account['username'],account['password']))
        # print ('rpm_data', rpm_resp.text)
        # rpm_pattern = 'filename=.*.src.rpm'
        # index = 10 + len(pkg) + 1
        # rpm_ver = re.search(rpm_pattern, rpm_resp.text).group()[index:-15]
        searchobj = re.search('filename=.*-(\d+.*\d+-\d+)\..*.src.rpm', rpm_resp.text)
        # print (searchobj.group())
        # print (searchobj.group(1))
        rpm_ver = searchobj.group(1)
        print (pkg, 'rpm_ver>>>', rpm_ver)


def get_obs_specver(url, account, project, packages):
    for pkg in packages:
        spec_url = '{}/package/view_file/{}/{}/_service:tar_scm:{}.spec?expand=1'.format(url, project, pkg, pkg)
        spec_resp = requests.get(spec_url,auth=HTTPBasicAuth(account['username'],account['password']))
        # print (spec_resp.text)
        searchobj = re.search(';Version:(.*);Release:(.*);Summary', spec_resp.text)
        # print (searchobj.group(0))
        # print (searchobj.group(1))
        # print (searchobj.group(2))
        ver = re.search('(\d+.*\d+)&#',searchobj.group(1)).group(1)
        release = re.search('(\d+)&#',searchobj.group(2)).group(1)
        # print (ver)
        # print (release)
        pkg_ver = '{}-{}'.format(ver,release)
        print (pkg,'spec pkg_ver>>>', pkg_ver)



if __name__=="__main__":
   obs_account = parameters.obs_account
   obs_url = parameters.obs_url
   obs_project = parameters.obs_project
   obs_packagelist = parameters.obs_packagelist
   obs_repo = parameters.obs_repo
   get_obs_rpmver(obs_url, obs_account, obs_project, obs_repo, obs_packagelist)
   get_obs_specver(obs_url, obs_account, obs_project, obs_packagelist)