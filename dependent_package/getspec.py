import requests
import json
# import base64
# import re
# import graphviz
import os


headers = {
    'Content-Type': 'application/json;charset=UTF-8'
    }
params = {
    'access_token': '1ff56996299ca3abb99cdbc2043401de'
}

specfilepath = os.path.join(os.getcwd(), 'specfile.json')
# buildrequire_filepath = os.path.join(os.getcwd(), 'buildrequiresfile.json')


def get_packages(headers, params):
    url = 'https://gitee.com/api/v5/repos/openEuler/RISC-V/contents/configuration/obs_meta/openEuler:Mainline:RISC-V'
    response = requests.get(url, headers=headers, params=params)
    packages = json.loads(response.text)
    # print ('packages length', len(packages))
    packagelist = [x['name'] for x in packages]
    print ('packagelist length', len(packagelist))
    return packagelist


def get_specfile(headers, params, packages, filepath):
    specfile_list = []
    for pkg in packages:
        print ('package', pkg, packages.index(pkg)+1)
        if pkg == 'rubygems-ronn':
           pkg = 'rubygem-ronn'
        url = 'https://gitee.com/api/v5/repos/src-openEuler/{}/contents'.format(pkg)
        response = requests.get(url, headers=headers, params=params)
        packagesource = json.loads(response.text)
        specfiles = [x['name'] for x in packagesource if x['name'].endswith('.spec')]
        print ('specfiles', specfiles)
        for spec in specfiles:
            specfile_api = 'https://gitee.com/api/v5/repos/src-openEuler/{}/contents/{}'.format(pkg, spec)
            specfile_dict = dict(package=pkg, specfile=spec, apiurl=specfile_api)
            specfile_list.append(specfile_dict)
        print ('specfile_list length', len(specfile_list))
        filepath = os.path.join(os.getcwd(), 'specfile.json')
        with open(filepath, 'w') as f:
            json.dump(specfile_list, f)


def get_buildrequires(headers, params, specfile, requirefile):
    with open(specfile, 'r') as f:
        specdata = json.load(f)
    print ('specdata length', len(specdata))
    # specdata = [{"specfile": "atune.spec", "apiurl": "https://gitee.com/api/v5/repos/src-openEuler/A-Tune/contents/atune.spec"}]
    requirepakcage_list = []
    for specfile in specdata:
        print ('specfile', specfile)
        # url = 'https://gitee.com/api/v5/repos/src-openEuler/{}/contents/{}'.format(package, specfile)
        response = requests.get(specfile['apiurl'], headers=headers, params=params)
        specfiledata = json.loads(response.text)
        # print (specfiledata)
        spec_content = base64.b64decode(specfiledata['content']).decode()
        # print ('spec_content', spec_content)
        specdata_list = spec_content.split('\n')
        # print ('specdata_list', specdata_list)
        namelist = [x for x in specdata_list if x.startswith('Name:') or x.startswith('name:')]
        namelist = [re.sub(r'\t', ' ', x) for x in namelist]
        # print ('namelist', namelist)
        packagename = list(filter(None, namelist[0].split(' ')))[1]
        print ('packagename', packagename)
        buildrequires = [x for x in specdata_list if x.startswith('BuildRequires:')]
        buildrequires = [x.replace('BuildRequires:', 'BuildRequires: ') for x in buildrequires]
        buildrequires = [re.sub(r'\t', ' ', x) for x in buildrequires]
        print ('buildrequire', buildrequires)
        requirespkg_list = []
        for item in buildrequires:
            itemlist = list(filter(None, item.split(' ')))
            print ('itemlist', itemlist)
            for i in range(len(itemlist)):
                print ('itemlist[i]', itemlist[i])
                m1 = re.match(r'^[^A-Za-z0-9_/-]*', itemlist[i]).group()
                m2 = re.match(r'^(\d\W)*', itemlist[i]).group()
                print ('match string1', m1)
                print ('match string2', m2)
                if m1 or m2:
                    itemlist[i] = ''
            itemlist.remove('BuildRequires:')
            itemlist = list(filter(None, itemlist))
            print ('newitemlist', itemlist)
            requirespkg_list = requirespkg_list + itemlist
        print ('requirespkg_list', requirespkg_list)
        requirespkg_dict = dict(name=packagename, buildrequires=requirespkg_list)
        print ('requirespkg_dict', requirespkg_dict)
        requirepakcage_list.append(requirespkg_dict)
        print ('requirepakcage_list length', len(requirepakcage_list))
    print ('requirepakcage_list length', len(requirepakcage_list))
    with open(requirefile, 'w') as f:
        json.dump(requirepakcage_list, f)

def create_graph(data):
    dot = graphviz.Digraph(comment='packges dependency table')
    for pkg in data['packages']:
        dot.node(pkg)
    for requirepkg in data['buildrequires']:
        for item in requirepkg['buildrequires']:
            dot.edge(item, requirepkg['name'])
    print (dot.source)
    dot.render('graph-output/packges-dependency-table.gv', view=True)


if __name__=="__main__":
    packagelist = get_packages(headers, params)
    get_specfile(headers, params, packagelist, specfilepath)
