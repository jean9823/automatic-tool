import requests
import json
import base64
import re

owner1 = 'openEuler'
owner2 = 'src-openEuler'
repo1 = 'RISC-V'
repo2 = 'A-Tune'
path1 = 'configuration/obs_meta/openEuler:Mainline:RISC-V'
# path2 = 'configuration/obs_meta/openEuler:Mainline:RISC-V/A-Tune/_service'
path3 = 'atune.spec'

headers = {
    'Content-Type': 'application/json;charset=UTF-8'
    }
url1 = 'https://gitee.com/api/v5/repos/{}/{}/contents/{}'.format(owner1, repo1, path1)
# url2 = 'https://gitee.com/api/v5/repos/{}/{}/contents/{}'.format(owner1, repo1, path2)
url2 = 'https://gitee.com/api/v5/repos/{}/{}/contents'.format(owner2, repo2)
url3 = 'https://gitee.com/api/v5/repos/{}/{}/contents/{}'.format(owner2, repo2, path3)

params = {
    'access_token': '1ff56996299ca3abb99cdbc2043401de'
}

# response1 = requests.get(url1, headers=headers, params=params)
# packages = json.loads(response1.text)
# print (len(packages))
# print (packages[0])

# response2 = requests.get(url2, headers=headers, params=params)
# packagesource = json.loads(response2.text)
# # print ('packagesource', packagesource)
# specfile = [x['name'] for x in packagesource if x['name'].endswith('.spec')][0]
# print (specfile)
# content = base64.b64decode(servicefile['content']).decode("utf-8")
# print ('content', content)
# partten = r'<param name="url">(.*?)</param>'
# stringlist = re.findall(partten, content)
# print ('string', stringlist[0])

# response3 = requests.get(url3, headers=headers, params=params)
# specfile = json.loads(response3.text)
# spec_content = base64.b64decode(specfile['content']).decode()
# print ('speccontent', spec_content)
# spec_stringlist = spec_content.split('\n')
# namelist = [x for x in spec_stringlist if x.startswith('Name:')]
# packagename = namelist[0].split(' ')[1]
# print ('packagename', packagename)
# buildrequires = [x for x in spec_stringlist if x.startswith('BuildRequires:')]
# print ('buildrequire', buildrequires)
# requirespkg_list = []
# for item in buildrequires:
#     itemlist = item.split(' ')
#     print ('itemlist', itemlist)
#     for x in itemlist:
#         m = re.match(r'^\W*', x).group()
#         print ('match string', m)
#         if m:
#             indexlist = [index for index,value in enumerate(itemlist) if value == m]
#             del itemlist[indexlist[0] + 1]
#             del itemlist[indexlist[0]]
#     itemlist.remove('BuildRequires:')
#     print ('newitemlist', itemlist)
#     requirespkg_list = requirespkg_list + itemlist
# print ('requirespkg_list', requirespkg_list)
# requirespkg_dict = dict(name=packagename, buildrequires=requirespkg_list)
# print ('requirespkg_dict', requirespkg_dict)


l1=['BuildRequires:', 'rpm-build', 'golang-bin', 'procps-ng']
l2=['BuildRequires:', 'sqlite', '>=!@<', '3.24.0', 'openssl', '>=', '2.2.0']
l3=['BuildRequires:', 'python3-scikit-optimize', 'python3-pandas', 'python3-xgboost']

for x in l2:
    m = re.match(r'^\W*', x).group()
    print ('match string', m)
    if m:
        indexlist = [index for index,value in enumerate(l2) if value == m]
        del l2[indexlist[0] + 1]
        del l2[indexlist[0]]
l2.remove('BuildRequires:')
        
print (l2)
