import requests
from bs4 import BeautifulSoup
from lxml import etree

username='wujie@nj.iscas.ac.cn'
url = 'https://gitee.com/login'
# token = 'ZZ5SaBnQ2M8puMZouOs8emJJzqS7wB2ZBNAr3/IKx0/JuL8Uvu7mMWq5kq/ZLhgmGktFm0T/EKIiownPogMT9w=='
# authenticity_token = '1ff56996299ca3abb99cdbc2043401de'
headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
            "Connection": "keep-alive",
            "Content-Type": "application/x-www-form-urlencoded",
            "Host": "gitee.com",
            "Origin": "https://gitee.com",
            "Referer": "https://gitee.com/login",
            'User-Agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36",
        }


session = requests.Session()
page_text = session.get(url=url, headers=headers).text
tree = etree.HTML(page_text)
authenticity_token = tree.xpath('//input[@name="authenticity_token"]/@value')[0]
print ('authenticity_token', authenticity_token)

postData = {
            "encrypt_key": "password",
            "utf8": "✓",
            "authenticity_token": authenticity_token,
            "redirect_to_url": "",
            "user[login]": username,
            "user[remember_me]": "0"
        }

resp = session.post(url, data=postData, headers=headers)
print ('resp status', resp.status_code)
cookie_dict = requests.utils.dict_from_cookiejar(session.cookies)
print ('cookie_dict', cookie_dict)
cookie = cookie_dict['gitee-session-n']


url = 'https://gitee.com/organizations/src-openeuler/pull_requests?assignee_id=&author_id=9486341&label_ids=&label_text=&milestone_id=&priority=&project_id=&project_type=&scope=&search=&single_label_id=&single_label_text=&sort=closed_at+desc&status=all&target_project=&tester_id='
headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Host': 'gitee.com',
        'Cookie': cookie
    }
response = requests.get(url, headers=headers)
# print(response.text)
soup = BeautifulSoup(response.text, 'html.parser')
# print (soup)
c = soup.find(class_="panel-list mt-2").findAll(class_='panel-list')
print (c)